# Pelatihan BERT Menggunakan Pendekatan Semi-Supervised untuk Aspect-Based Sentiment Analysis Dengan Dataset Restaurant Review Bahasa Indonesia

Repositori ini adalah *source code* untuk TK1 Penambangan Data Kelompok 2. Repositori berisi *notebook* untuk pelatihan semua model yang dibuat pada laporan (kecuali model IndoBERT dasar), program untuk *preprocessing data*, serta pengumpulan metrik dari *testing data*. Pelatihan model dilakukan dengan Google Colab kecuali 

## Preprocessing

Dataset hasil *preprocessing* dapat diakses dalam bentuk *tar file* pada link [ini](https://drive.google.com/uc?id=1UMQeQG2wBfH0WMwf_EMkii6Ijn58MVmE). Preprocessing dilakukan menggunakan 2 *Python script* yang terdapat pada direktori preprocessing. Tahapan preprocessing melakukan ekstraksi dari dataset awal (dalma bentuk XML) dan mengumpulkannya dalam bentuk CSV file.

Untuk menjalankan program *preprocessing*, simpan dataset XML awal pada *root directory* pada repositori ini.

```
cd preprocessing
python preprocess.py --labelled-data /path/to/labelled_data.xml --unlabelled-data /path/to/unlabelled_data.xml
python preprocess_test.py --test-data /path/to/testing_data.xml
```

Hasil dari penjalanan kedua program di atas dapat dikompres untuk pelatihan model lebih lanjut:

```
tar -cjf dataset.tar labeled.csv unlabeled.csv test.csv
```

## GAN-IndoBERT

Desain model dapat dibaca pada laporan utama.

Implementasi GAN-IndoBERT pada repositori ini menggunakan Cloud TPU (Tensor Processing Unit). Akses terhadap TPU resource dapat dibuka pada Google Colab dengan memilih TPU pada Runtime > Change Runtime Type.