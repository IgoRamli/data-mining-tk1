import csv
from argparse import ArgumentParser
from lxml import etree
from random import shuffle

parser = ArgumentParser(description='Preprocess Restaurant Review data')
parser.add_argument('--labelled-data')
parser.add_argument('--unlabelled-data')
aspects = []

def interpret_polarity(polarity):
  polarity = polarity.lower().strip()
  if polarity[0] == 'p':
    return 'POSITIVE'
  if polarity[0] == 'n':
    return 'NEGATIVE'
  assert(False)

def dump_to_csv(csvfile, all_reviews):
  columns= ['text'] + list(aspects)
  writer = csv.DictWriter(csvfile, fieldnames=columns, delimiter='|')
  writer.writeheader()
  for row in all_reviews:
    writer.writerow(row)

def parse_labelled_data(in_file, out_file):
  xml_parser = etree.XMLParser(recover=True)
  tree = etree.parse(in_file, parser=xml_parser)
  corpus = tree.getroot()

  # First, get all aspects
  if len(aspects) == 0:
    for aspect in corpus.iter('aspect'):
      aspects.add(aspect.attrib['category'])

    print('| {} aspects found'.format(len(aspects)))
    for i in aspects:
      print('| -', i)

  # Parse all aspects for each review
  all_reviews = []
  for review in corpus.iter('review'):
    text = review[0].text
    review_aspects = review.iter('aspect')
      
    ratings = { 
      aspect: 'NEUTRAL'
      for aspect in aspects
    }
    for aspect in review_aspects:
      category = aspect.attrib['category']
      rating = aspect.attrib['polarity']
      ratings[category] = interpret_polarity(rating)
    processed = {
      'text': text,
      **ratings
    }
    all_reviews.append(processed)
  print('| {} rows found'.format(len(all_reviews)))
  shuffle(all_reviews)
  dump_to_csv(out_file, all_reviews)
    
def parse_unlabelled_data(in_file, out_file):
  xml_parser = etree.XMLParser(recover=True)
  tree = etree.parse(in_file, parser=xml_parser)
  corpus = tree.getroot()

  # Parse all aspects for each review
  all_reviews = []
  for review in corpus.iter('review'):
    text = review.text
    
    ratings = { 
      aspect: 'UNKNOWN'
      for aspect in aspects
    }
    processed = {
      'text': text,
      **ratings
    }
    all_reviews.append(processed)
  print('| {} rows found'.format(len(all_reviews)))
  shuffle(all_reviews)
  dump_to_csv(out_file, all_reviews)
  
if __name__ == '__main__':
  args = parser.parse_args()
  all_reviews = []
  aspects = set()
  if args.labelled_data is not None:
    with open(args.labelled_data, 'r') as labelled:
      with open('labeled.csv', 'w+') as out_file:
        print('| Parsing {}'.format(args.labelled_data))
        parse_labelled_data(labelled, out_file)

  if args.unlabelled_data is not None:
    with open(args.unlabelled_data, 'r') as unlabelled:
      with open('unlabeled.csv', 'w+') as out_file:
        print('| Parsing {}'.format(args.unlabelled_data))
        parse_unlabelled_data(unlabelled, out_file)
