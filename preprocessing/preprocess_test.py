import csv
from argparse import ArgumentParser
from lxml import etree
from random import shuffle

parser = ArgumentParser(description='Preprocess Test data')
parser.add_argument('--test-data')

aspects = ['PRICE', 'AMBIENCE', 'SERVICE', 'FOOD']

def interpret_polarity(polarity):
  polarity = polarity.lower().strip()
  if polarity[0] == 'p':
    return 'POSITIVE'
  if polarity[0] == 'n':
    return 'NEGATIVE'
  assert(False)

def dump_to_csv(csvfile, all_reviews):
  columns= ['id', 'text'] + list(aspects)
  writer = csv.DictWriter(csvfile, fieldnames=columns, delimiter='|')
  writer.writeheader()
  for row in all_reviews:
    writer.writerow(row)
    
def parse_test_data(in_file, out_file):
  xml_parser = etree.XMLParser(recover=True)
  tree = etree.parse(in_file, parser=xml_parser)
  corpus = tree.getroot()

  # Parse all aspects for each review
  all_reviews = []
  for row in corpus.iter('row'):
    idx = row.find('index').text
    review = row.find('review')
    text = review.text
    
    ratings = { 
      aspect: 'UNKNOWN'
      for aspect in aspects
    }
    processed = {
      'id': idx,
      'text': text,
      **ratings
    }
    all_reviews.append(processed)
  print('| {} rows found'.format(len(all_reviews)))
  dump_to_csv(out_file, all_reviews)
  
if __name__ == '__main__':
  args = parser.parse_args()

  if args.test_data is not None:
    with open(args.test_data, 'r') as test:
      with open('test.csv', 'w+') as out_file:
        print('| Parsing {}'.format(args.test_data))
        parse_test_data(test, out_file)
